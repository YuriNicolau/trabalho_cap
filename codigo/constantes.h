#ifndef CONSTANTES_H
#define CONSTANTES_H

#define QUANT_PALAVRAS 0.037 	//multiplicador - palavras = linhas*colunas*multiplicador
#define TAM_PALAVRAS 15 	//tamanho maximo das palavras
#define TAM_MIN 4		//Tamanho Minimo para a palavra na lista de palavras
#define MIN_LINHAS 20		//Minimo de linhas para a matriz de caracteres a ser gerada
#define MIN_COLUNAS 20		//Minimo de colunas para a matriz de caracteres a ser gerada
#define MAX_SOBREPOSTAS 1	//Maximo de caracteres sobrepostos entre duas palavras
#define VERT 1 //direção vertical
#define HORI 0 //direção horizontal

/*
 Definições de estruturas de dados para a matriz e a lista de respostas
*/

typedef struct{
	char **matriz; 	//O endereço da matriz
	int linhas; 	//A quantidade de linhas da matriz
	int colunas; 	//A quantidade de colunas da matriz
}tMatriz;

typedef struct{
	tMatriz matRespostas;	//Estrutura de matriz para matriz de respostas
	char **lista;		//Lista de palavras a serem encontradas na matriz
	int *encontradas;	//Vetor associado a lista para indicar as palvras encontradas na matriz
	int quantPalavras;	//quantidade palavras da lista de palavras
	int tamPalavras;	//Tamanho máximo para cada palavra da lista de palavras
}tRespostas;

#endif
