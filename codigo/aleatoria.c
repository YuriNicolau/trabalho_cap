//Bibliotecas
#include<stdio.h>
#include<string.h>

//Bibliotecas próprias
#include "aleatoria.h"
#include "geral.h"
#include "constantes.h"


/*
Objetivo: Lê uma lista de palavras digitadas pelo usuário
Parâmetros formais: 
 - resp: Variável do tipo tRespostas, onde serão armazenadas as palavras
*/
void lePalavras(tRespostas *resp){
	int i;
	resp->tamPalavras = TAM_PALAVRAS;
	resp->lista = alocaMatriz(resp->quantPalavras,resp->tamPalavras);
	printf("Digite as %d palavras!\n",resp->quantPalavras);
	for(i=0;i<resp->quantPalavras;i++){
		do{
			printf("Digite a %da palavra (%d a %d caracteres): ",i+1,TAM_MIN,resp->tamPalavras);
			fgets(resp->lista[i],resp->tamPalavras,stdin);
		}while(!trataString(resp->lista[i]));
	}
}

/*
Objetivo: Incluir, de maneira aleatória, as palavras de uma lista em uma matriz
Parâmetros formais: 
 - mat: Variável do tipo tMatriz, contendo a matriz onde a lista de palavras será inserida
 - resp: Variável do tipo tRespostas, contendo a lista de palavras a ser inserida
*/
void incluiPalavras(tMatriz *mat,tRespostas *resp){
	int tam,i,j,l,c,dx,dy,valido,sobrepostas;

	resp->matRespostas.linhas = mat->linhas;
	resp->matRespostas.colunas = mat->colunas;

	//Cria e inicializa uma matriz para a localização das respostas
	resp->matRespostas.matriz = alocaMatriz(mat->linhas,mat->colunas);
	for(i=0;i<mat->linhas;i++){
		for(j=0;j<mat->colunas;j++){
			resp->matRespostas.matriz[i][j] = '.';	
		}
	}

	for(i=0;i<resp->quantPalavras;i++){
		do{
			dx = -1+aleatorio(3);
			dy = -1+aleatorio(3);
		}while(!(dx*dx+dy*dy));//Enquanto ambos forem iguais a zero
		
		tam = strlen(resp->lista[i]);

		do{
			l = aleatorio(mat->linhas);
			c = aleatorio(mat->colunas);
		}while(!validez(resp->matRespostas,l,c,resp->lista[i],dx,dy));
		
		for(j=0;j<tam;j++){
			mat->matriz[l+dy*j][c+dx*j] = resp->lista[i][j];
			resp->matRespostas.matriz[l+dy*j][c+dx*j] = mat->matriz[l+dy*j][c+dx*j];
		}
	}
}

/*
Objetivo: Gerar uma lista de palavras aleatórias (mesmo que sem sentido),
à partir de uma matriz pré-existente
Parâmetros formais: 
 - mat: Variável to tipo tMatriz, onde estão as palavras a serem encontradas
 - resp: Variável do tipo tRespostas de palavras onde serão armazenadas as palavras aleatórias encontradas
*/
void geraLista(tMatriz *mat,tRespostas *resp){
	int tam,i,j,l,c,dir;

	resp->tamPalavras = TAM_PALAVRAS;
	resp->quantPalavras = (int)mat->linhas*mat->colunas*QUANT_PALAVRAS;
	resp->lista = alocaMatriz(resp->quantPalavras,resp->tamPalavras);
	
	resp->matRespostas.linhas = mat->linhas;
	resp->matRespostas.colunas = mat->colunas;
	inicializaMatRespostas(&resp->matRespostas);

	for(i=0;i<resp->quantPalavras;i++){
		tam = aleatorio(4)+4;
		for(j=0;j<tam;j++){
			resp->lista[i][j] = 'A' + aleatorio(26);
		}
		resp->lista[i][j] = '\0';
	}
}

/*
Objetivo: Verificar a validez do posicionamento da palavra na matriz
(Se a palavra fica toda dentro da matriz e não altera nenhuma outra palavra)
Parametros Formais:
 - mat: Variavel do tipo tMatriz, onde está localizada a matriz cuja a palavra será inserida
 - l: linha onde o primeiro char da palavra será inserido
 - c: coluna onde o primeiro char da palavra será inserido
 - palavra: ponteiro para a palavra a ser verificada a validez de seu posicionamento
 - dir: direção de alocação da palavra (0=Horizontal e 1=Vertical)
Valor de Retorno:
 -Validez: 1 se o posicionamento da palavra é valido e 0 se não
*/
int validez(tMatriz mat,int l,int c,char *palavra,int dx,int dy){	
	int i,sobrepostas,valido,tam;

	tam = strlen(palavra);

	//para evitar que duas palavras fiquem muito sobrepostas
	sobrepostas = 0;//Verifica quantas letras estão sobrepostas. O máximo admitido é uma MAX_SOBREPOSTAS

	valido = 0;
	//Para não extrapolar a matriz
	if(l+dy*(tam-1)>=0 && l+dy*(tam-1)<mat.linhas && c+dx*(tam-1)>=0 && c+dx*(tam-1)<mat.colunas)
		valido = 1;

	if(valido){
		for(i=0;i<tam;i++){
			if(mat.matriz[l+dy*i][c+dx*i] != '.'){
				sobrepostas++;
				if(mat.matriz[l+dy*i][c+dx*i] != palavra[i]){
					valido = 0;
					break;
				}
			}
			if(sobrepostas>MAX_SOBREPOSTAS){
				valido = 0;
				break;
			}
		}
	}
	return valido;
}

/*
Objetivo: Deixar uma string no formato padrão trabalhado pelo programa
Parametros Formais:
 - string: ponteiro para a string a ser tratada
Valor de Retorno:
 - 0 se a string não é valida e 1 se ela for válida
*/
int trataString(char *string){
	int i=0;
	while(string[i]!='\0'){
		if(string[i] == '\n')
			string[i] = '\0';
		else{
			if(string[i]>='a' && string[i]<='z')
				string[i] += 'A'-'a';
			else if(!(string[i]>='A' && string[i]<='Z')){
				printf("Caractere inválido detectado!\n");
				return 0;
			}
			i++;
		}
	}
	if(strlen(string)<TAM_MIN){
		printf("Digite pelo menos %d caracteres!\n",TAM_MIN);
		return 0;
	}
	return 1;
}

/*
Objetivo: Gerar Uma Matriz aleatoria com tamanho definido pelo usuário
Parametros Formais:
 - mat: Ponteiro para a variavel do tipo tMatriz, onde será armazenada a matriz gerada
*/
void matrizAleatoria(tMatriz *mat){
	int i,j,linhas,colunas;
	printf("Quantidade de linhas: ");
	scanf("%d",&linhas);
	while(linhas<MIN_LINHAS){
		printf("Tamanho inválido!\nDigite um número maior ou igual a %d\n",MIN_LINHAS);
		scanf("%d",&linhas);
	}
	printf("Quantidade de colunas: ");
	scanf("%d",&colunas);
	while(colunas<MIN_COLUNAS){
		printf("Tamanho inválido!\nDigite um número maior ou igual a %d\n",MIN_COLUNAS);
		scanf("%d",&colunas);
	}
	mat->linhas = linhas;
	mat->colunas = colunas;

	mat->matriz = alocaMatriz(mat->linhas,mat->colunas);

	for(i=0;i<mat->linhas;i++){
		for(j=0;j<mat->colunas;j++){
			mat->matriz[i][j] = 'A'+aleatorio(26);
			if(mat->matriz[i][j] == mat->matriz[i][j-1])
				mat->matriz[i][j] = 'A'+aleatorio(26);
		}
	}
}
