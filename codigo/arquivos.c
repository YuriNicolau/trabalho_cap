//Bibliotecas
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

//Bibliotecas Próprias
#include "constantes.h"
#include "arquivos.h"
#include "geral.h"

/*
Objetivo:Ler Matriz de Caracteres do arquivo inserido pelo usuário através de um arquivo
Parametros Formais:
 - mat: Ponteiro para a variavel do tipo tMatriz onde será armarzenada a Matriz lida
*/
void leMatriz(tMatriz *mat){
	FILE *arq;
	char caminho[256],caractere;
	int i,j;

	do{
		printf("Digite o nome ou endereço do arquivo da matriz: ");
		fgets(caminho,256,stdin);
		i=0;
		while(caminho[i] != '\0'){
			if(caminho[i] == '\n')
				caminho[i] = '\0';
			else
				i++;
		}

		arq = fopen(caminho,"r");
		if(!arq)
			printf("Caminho inválido!\n");
	}while(!arq);

	fscanf(arq,"%d",&mat->linhas);
	fscanf(arq,"%d",&mat->colunas);
	
	mat->matriz = alocaMatriz(mat->linhas,mat->colunas);

	i=0,j=0;
	while(fscanf(arq,"%c",&caractere)!=EOF){
		if(caractere == '\n' && j!=0){
			i++;
			j=0;
		}else if(caractere!=' ' && caractere != '\n'){
			mat->matriz[i][j] = caractere;
			j++;
		}
	}
}

/*
Objetivo: Ler lista de Palavras inseridas pelo usuário através de um arquivo
Parametros Formais:
 - resp: Ponteiro para a variavel do tipo tRespostas onde será armazenada a lista de palavras
*/
void leLista(tRespostas *resp){
	FILE *arq;
	char caminho[256],caractere;
	int i,j;
	do{
		printf("Digite o nome ou endereço do arquivo da lista de palavras: ");
		fgets(caminho,256,stdin);
		i=0;
		while(caminho[i]!='\0'){
			if(caminho[i] == '\n')
				caminho[i] = '\0';
			else
				i++;
		}

		arq = fopen(caminho,"r");
		if(!arq)
			printf("Caminho inválido!\n");
	}while(!arq);
	fscanf(arq,"%d",&resp->quantPalavras);

	resp->lista = alocaMatriz(resp->quantPalavras,40);
	//getchar();
	for(i=0;i<resp->quantPalavras;i++){
		j=0;
		do{
			fscanf(arq,"%c",&caractere);
			if(caractere!='\n'){
				resp->lista[i][j]=caractere;
				j++;
			}
		}while(caractere!='\n' || j==0);
		resp->lista[i][j]='\0';
	}
}

/*
Objetivo: Encontrar palavras a partir de uma lista em uma Matriz de caracteres
Parametros Formais:
 - resp : Ponteiro para a variavel do tipo tRespostas, de onde será lida a lista de palavras e será armazenada
quais foram ou não encontradas
 - mat : Ponteiro para a variavel do tipo tMatriz, de onde será lida a matriz de caracteres onde as palavras
serão buscadas
*/
void encontraPalavras(tRespostas *resp, tMatriz *mat){
	int i,j,k,x,dx,dy,tamPalavra,encontrada;
	resp->matRespostas.linhas = mat->linhas;
	resp->matRespostas.colunas = mat->colunas;
	resp->matRespostas.matriz = alocaMatriz(resp->matRespostas.linhas,resp->matRespostas.colunas);
	inicializaMatRespostas(&resp->matRespostas);

	resp->encontradas = (int*)malloc(resp->quantPalavras*sizeof(int));

	//Para cada palavra
	for(i=0;i<resp->quantPalavras;i++){
		tamPalavra = strlen(resp->lista[i]);
		resp->encontradas[i] = 0;
		for(j=0;j<mat->linhas;j++){
			for(k=0;k<mat->colunas;k++){
				//Verifica se a primeira letra da palavra foi encontrada
				if(resp->lista[i][0]==mat->matriz[j][k]){
					//Encontra para qual direção a palavra continua
					encontrada=0;
					for(dx=-1;dx<=1 && !encontrada;dx++){
						for(dy=-1;dy<=1 && !encontrada;dy++){
							//Verificação para não extrapolar a matriz
							if((dy||dx) && j+dy*(tamPalavra-1)>=0 && j+dy*(tamPalavra-1)<mat->linhas && k+dx*(tamPalavra-1)>=0 && k+dx*(tamPalavra-1)<mat->colunas){
								//Verificação letra a letra
								encontrada = 1;
								for(x=1;x<tamPalavra && encontrada;x++){
									if(resp->lista[i][x]!=mat->matriz[j+dy*x][k+dx*x])
										encontrada = 0;
								}
							}
						}
					}
					if(encontrada){
						dx = dx-1;
						dy = dy-1;
						resp->encontradas[i] = 1;
						for(x=0;x<tamPalavra;x++){
								resp->matRespostas.matriz[j+x*dy][k+x*dx] = mat->matriz[j+x*dy][k+x*dx];
						}
					}
				}
			}
		}
	}
}
