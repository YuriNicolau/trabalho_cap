//Bibliotecas
#include<stdio.h>
#include<stdlib.h>
#include<time.h>

//Bibliotecas próprias
#include "geral.h"
#include "constantes.h"

/*
Objetivo: Inicializa matriz de respostas, preenchendo todo os valores com '.'
Parametros Formais:
 - mat: Ponteiro para a variavel do tipo tMatriz, onde será armazenada a matriz inicializada
*/
void inicializaMatRespostas(tMatriz *mat){
	int i,j;
	mat->matriz = alocaMatriz(mat->linhas,mat->colunas);

	//Cria e inicializa uma matriz para a localização das respostas
	for(i=0;i<mat->linhas;i++){
		for(j=0;j<mat->colunas;j++){
			mat->matriz[i][j] = '.';	
		}
	}

}

/*
Objetivo: Alocar memória para uma matriz dinamicamente
Parametros Formais:
 - linhas: número de linhas para a matriz a ser alocada
 - colunas: número de colunas para a matriz a ser alocada
Valor de Retorno:
 - Endereço do primeiro valor da matriz alocada
*/
char **alocaMatriz(int linhas,int colunas){
	int i;
	char **mat;
	mat = malloc(linhas*sizeof(char *));
	for(i=0;i<linhas;i++){
		mat[i] = malloc(colunas*sizeof(char));
	}

	return mat;
}

/*
Objetivo: Gerar um char aleatório
Parametros Formais:
- n: Escopo da geração aleatória, o valor X gerado será   0 <= X < n
Valor de Retorno:
- o Valor do char gerado aleatoriamente, dentro do escopo n
*/
char aleatorio(int n){
	srand(time(NULL)*rand());
	return (rand()%n);
}

/*
Objetivo:Escrever na tela para o usuario a lista de palavras, arcando quais foram ou
não encontradas
Parametros Formais:
- resp: Variavel do tipo tRespostas onde está armazenada a lista de palavras e se foram ou não 
encontradas na matriz de caracteres
*/
void printaEncontradas(tRespostas resp){
	int i;
	printf("\n");
	for(i=0;i<resp.quantPalavras;i++){
		printf("%s",resp.lista[i]);
		if(resp.encontradas[i])
			printf("\t - Encontrada!\n");
		else
			printf("\t - Não encontrada!\n");
	}
}

/*
Objetivo: Escrever a lista de palavras na tela para o usuário
Parametros Formais:
-resp : Variavel do tipo tRespostas onde se encontra armazenada a ista de palavras
*/
void printaLista(tRespostas resp){
	int i;
	printf("\n");
	for(i=0;i<resp.quantPalavras;i++){
		printf("%s\n",resp.lista[i]);
	}
}

/*
Objetivo: Escrever uma matriz na tela para o usuário
Parametros Formais:
- mat: Variavel do tipo tMatriz que contenha a matriz a ser escrita
*/
void printaMatriz(tMatriz mat){
	int i,x;
	printf("\n");
	for(i=0;i<mat.linhas;i++){
		for(x=0;x<mat.colunas;x++){
			printf("%-2c",mat.matriz[i][x]);
		}
		printf("\n");
	}
}

/*
Objetivo: Escrever as estatísticas do caça-palavras
Parametros Formais:
- resp: Variável do tipo tRespostas, de onde serão extraídas
as estatísticas
*/
void printaEstatisticas(tRespostas resp){
	printf("%.2lf%% das palavras encontradas\n",porcentagemEncontradas(resp)*100);
}

/*
Objetivo: Calcular as estatísticas do caça-palavras
Parametros Formais:
- resp: Variável do tipo tRespostas, de onde serão extraídas
as estatísticas
*/
double porcentagemEncontradas(tRespostas resp){
	int i;
	double pEncontradas; //Porcentagem de palavras encontradas
	pEncontradas = 0;

	for(i=0;i<resp.quantPalavras;i++)
		pEncontradas += resp.encontradas[i];
	pEncontradas = pEncontradas/(double)resp.quantPalavras;

	return pEncontradas;
}
