/*
Autores: Yuri Nicolau Freire, Guilherme José Da Silva, Eric Sales Vitor Junior
Revisor: Professor Ricardo Rodrigues Ciferri
Data: 27/05/2018

Objetivo: Criar um programa que gere e veriique a validade de caça-palavras
*/

//Bibliotecas
#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<string.h>

//Bibliotecas próprias
#include "constantes.h"
#include "geral.h"
#include "aleatoria.h"
#include "arquivos.h"

/*
 Protótipos das funções
*/
int chamaMenu();
void verEstatisticas(tRespostas respostas);
void verRespostas(tRespostas respostas);
int corrigirCacaPalavras(tMatriz *matriz,tRespostas *respostas);

/*
 Função main, responsável pelo fluxo do programa e
por chamar as demais funções.
*/
int main(){
	int op,linhas,colunas,quantPalavras,temp;
	char tempChar;
	tMatriz matriz;
	tRespostas respostas;
	do{
		op = chamaMenu(); 
		getchar();
		switch(op){
			case 1:
				leMatriz(&matriz);
				leLista(&respostas);
				encontraPalavras(&respostas,&matriz);
				printaMatriz(matriz);
				printaEncontradas(respostas);
				
				verEstatisticas(respostas);
				verRespostas(respostas);
				
				if(porcentagemEncontradas(respostas)<1.0)
					if(corrigirCacaPalavras(&matriz,&respostas)){
						printaMatriz(matriz);
						verRespostas(respostas);
					}
	
				free(matriz.matriz);
				free(respostas.matRespostas.matriz);
				free(respostas.lista);
				free(respostas.encontradas);
				break;
			case 2:
				matrizAleatoria(&matriz);
				getchar();
				leLista(&respostas);
				incluiPalavras(&matriz,&respostas);
				printaLista(respostas);
				printaMatriz(matriz);
				printaMatriz(respostas.matRespostas);
				free(matriz.matriz);
				free(respostas.matRespostas.matriz);
				free(respostas.lista);
				break;
			
			case 3:
				matrizAleatoria(&matriz);
				printf("Digite a quantidade de palavras: ");
				scanf("%d",&respostas.quantPalavras);
				getchar();
				lePalavras(&respostas);
				incluiPalavras(&matriz,&respostas);
				printaLista(respostas);
				printaMatriz(matriz);
				printaMatriz(respostas.matRespostas);
				free(matriz.matriz);
				free(respostas.matRespostas.matriz);
				break;
			case 4:
				matrizAleatoria(&matriz);
				respostas.quantPalavras = (int)matriz.linhas*matriz.colunas*QUANT_PALAVRAS;

				geraLista(&matriz,&respostas);
				incluiPalavras(&matriz,&respostas);

				printaLista(respostas);
				printaMatriz(matriz);
				printaMatriz(respostas.matRespostas);
				
				free(matriz.matriz);
				free(respostas.matRespostas.matriz);
				break;
			case 0:
				printf("\nAdeus!\n\n");
				return 0;
				break;
			default:
				printf("Opção iválida!\n");
				break;
		}
	}while(op!=0);

	return 0;
}

/*
Objetivo: Imprimir na tela uma lista de opções do menu principal
do programa para o usuário e ler a opção escolhida
Parâmetros formais: 
 - A função não recebe nenhum parâmetro formal
Valor de retorno:
 - O caractere com a opção escolhida
*/
int chamaMenu(){
	int op;
	printf("\nEscolha uma das opções:\n");
	printf("1 - Encontrar palavras de uma lista em uma matriz fornecida\n");
	printf("2 - Entrar com um arquivo com a lista de palavras e gerar matriz\n");
	printf("3 - Digitar lista de palavras e gerar matriz\n");
	printf("4 - Gerar lista e matriz de palavras aleatoriamente\n");
	printf("0 - Terminar execução do programa\n");
	scanf("%d",&op);	
	return op;
}

/*
Objetivo: Verificar se o usuário deseja ver as estatísticas do caça-palavras
Parâmetros formais: 
 - respostas: Variável do tipo tRespostas, de onde serão extraídas as estatísticas
*/

void verEstatisticas(tRespostas respostas){
	char tempChar;

	printf("Deseja ver as estatísticas do caça-palavras? (s/n): ");
	tempChar = getchar();
	if(tempChar == 'S' || tempChar == 's')
		printaEstatisticas(respostas);
	else
		printf("Ok! :)\n");
	getchar();
}

/*
Objetivo: Verificar se o usuário deseja ver as respostas do caça-palavras
Parâmetros formais: 
 - respostas: Variável do tipo tRespostas, onde está a matriz
contendo a localização das palavras
*/

void verRespostas(tRespostas respostas){
	char tempChar;

	printf("Deseja ver a posição das palavras? (s/n): ");
	tempChar = getchar();
	if(tempChar == 'S' || tempChar == 's')
		printaMatriz(respostas.matRespostas);
	else
		printf("Ok! ^^\n");
	getchar();
}

/*
Objetivo: Verificar se o usuário deseja corrigir o caça-palavras
Parâmetros formais: 
 - respostas: Variável do tipo tRespostas, onde serão colocadas as
respostas da nova matriz
 - matriz: Variável do tipo tMatriz, onde será armazenada a nova matriz
Valor de retorno:
 - 1 se a matriz tiver sido corrigida, 0 se não
*/
int corrigirCacaPalavras(tMatriz *matriz,tRespostas *respostas){
	char tempChar;

	printf("Deseja corrigir o caça-palavras para incluir as palavras faltantes?(s/n): ");
	tempChar = getchar();
	if(tempChar == 'S' || tempChar == 's'){
		matrizAleatoria(matriz);
		getchar();
		incluiPalavras(matriz,respostas);
		return 1;
	}else{
		printf("Ok! ^^\n");
		return 0;
	}
}
