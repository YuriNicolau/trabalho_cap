#ifndef ALEATORIA_H
#define ALEATORIA_H
//Bibliotecas pŕoprias
#include "constantes.h"
//Protótipos das funções
void lePalavras(tRespostas *resp);
void incluiPalavras(tMatriz *mat,tRespostas *resp);
void geraLista(tMatriz *mat,tRespostas *resp);
int validez(tMatriz mat,int l,int c,char *palavra,int dx,int dy);
int trataString(char *string);
void matrizAleatoria(tMatriz *mat);

#endif
