#ifndef GERAL_H
#define GERAL_H
//Bibliotecas
#include "constantes.h"
//Protótipos das funções
void inicializaMatRespostas(tMatriz *mat);
char **alocaMatriz(int linhas,int colunas);
char aleatorio(int n);
void printaEncontradas(tRespostas resp);
void printaLista(tRespostas resp);
void printaMatriz(tMatriz mat);
void printaEstatisticas(tRespostas resp);
double porcentagemEncontradas(tRespostas resp);

#endif
