#ifndef ARQUIVOS_H
#define ARQUIVOS_H
//Bibliotecas Próprias
#include "constantes.h"
//Protótipos das funções
void leMatriz(tMatriz *mat); 
void leLista(tRespostas *mat);
void encontraPalavras(tRespostas *resp, tMatriz *mat);

#endif 
